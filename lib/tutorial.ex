defmodule Tutorial do
  alias Tutorial.HotelList
  alias Tutorial.Hotel
  alias Tutorial.Pager

  def base_url() do
    "http://seven.travel"
  end

  def http_get(url) do
    %HTTPoison.Response{status_code: 200, body: body} = HTTPoison.get!(base_url <> url, [],
      [recv_timeout: :infinity, timeout: :infinity])
    body
  end

  def run() do
    IO.puts "Запуск"
    HTTPoison.start
    hotel_list_links = HotelList.get_links()
    hotel_links_list_length = length(hotel_list_links)
    IO.puts "Всего отелей #{hotel_links_list_length}"
    Enum.each(hotel_list_links, fn(hotel_link) ->
      parent = self()
      Task.start_link(fn ->
        Hotel.parse(hotel_link)
        send(parent, {:parse_ok})
      end)
    end)
    IO.puts "Процессы созданы, ожидаем выполнения"
    Enum.each(1..hotel_links_list_length, fn(i) ->
      receive do
        {:parse_ok} -> IO.puts "Процес №#{i} выполнен."
      end
    end)
    IO.puts "Все задачи успешно выполнены"
  end
end
