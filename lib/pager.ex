defmodule Tutorial.Pager do
  #Получает количество страниц от пагинатора
  def page_count() do
    body = Tutorial.http_get("/hotels/")
    body |> Floki.find(".modern-page-navigation a")
      |> Enum.at(-2) |> Floki.text |> String.to_integer
  end

  #Возвращает массив ссылок пагинации
  def pager_links(page_count) do
    IO.puts "Всего #{page_count} страниц"
    for page_number <- 1..page_count do
      Tutorial.base_url() <> "/hotels/?PAGEN_1=#{page_number}"
    end
  end
end
