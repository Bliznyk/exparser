defmodule Tutorial.HotelList do
  alias Tutorial.Pager

  # Возвращает массив ссылок на страницы отелей
  def get_links() do
    pages = Pager.page_count();
    Pager.pager_links(pages) |> Enum.each(fn(pager_link) ->
      parent = self()
      Task.start_link(fn ->
        send(parent, {:hotel_links, hotel_links(pager_link)})
      end)
    end)
    for i <- 1..pages do
      receive do
        {:hotel_links, links} -> links
      end
    end |> List.flatten
  end

  #Парсит массив ссылок отелей с страницы пагинации
  defp hotel_links(page) do
    %{body: body} = HTTPoison.get!(page, [],
      [recv_timeout: :infinity, timeout: :infinity])
    IO.puts "Зашли на страницу #{page}"
    body |> Floki.find("#hotels .item .title a")
         |> Floki.attribute("href")
  end
end
