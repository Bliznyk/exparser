defmodule Tutorial.Hotel do
  # Парсит данные отеля по полученным относительным ссылкам
  def parse(link) do
    IO.puts "Заходим на #{Tutorial.base_url <> link}"
    content = Tutorial.http_get(link) |> Floki.find("#content")
    #Описание
    description = content |> Floki.find("#d_text")
      |> Floki.filter_out("iframe")
    #Строка адреса
    address = content |> Floki.find("#blocks .place")
      |> Floki.filter_out("a") |> Floki.text |> to_utf
      |> String.split("В •В", trim: true)
      |> Enum.map(&String.strip(&1))
    #Город
    title = content |> get_text_by_selector("h1.mrg10") |> to_utf
    city = if Enum.at(address, -1) == title do
      Enum.at(address, -2)
    else
      Enum.at(address, -1)
    end
    #Инфраструктура
    services = content |> Floki.find("#infra li") |> Enum.map(fn(service) ->
      service |> Floki.text |> to_utf |> String.strip
    end) |> Enum.join(", ")
    #Комнаты
    rooms = content |> Floki.find("#rooms .n_item")
    |> Enum.map(fn(room) ->
      %{
        title: room |> get_text_by_selector(".title") |> to_utf,
        price: room |> get_text_by_selector(".price") |> to_utf,
      }
    end)
    #Координаты
    map_string = content |> Floki.find("#blocks .place .link")
      |> Floki.attribute("href") |> Enum.at(0)
    map = if map_string do
      map_matches = Regex.scan(~r/\d+\.\d+/, map_string) |> List.flatten
      %{
        lat: map_matches |> Enum.at(0) |> String.to_float,
        lon: map_matches |> Enum.at(1) |> String.to_float
      }
    else
      %{}
    end

    json = %{
      base: %{
        title: title |> String.strip,
        city: city,
        address:  address |> Enum.join(", "),
        description: description |> Floki.text |> to_utf |> clear_string,
        link: Tutorial.base_url <> link,
        likes: content |> get_text_by_selector(".counters .likes font")
                       |> String.to_integer,
        how_came_in: content |> get_text_by_selector("#how_block")
          |> to_utf |> clear_string,
        services: services,
        map: map
      },
      rooms: rooms,
    }
    save_path = Path.absname(Path.relative(link))
    File.mkdir_p!(save_path)
    File.write!("#{save_path}/data.json", Poison.Encoder.encode(json, []))
    download_images(content, save_path)
    json
  end

  defp to_utf(string) do
    :iconv.convert("windows-1251", "utf-8", string)
  end

  defp clear_string(string) do
    string |> String.replace(~r/(В )?[\r\n\t]+( В)?/, "") |> String.strip
  end

  defp get_text_by_selector(content, selector) do
    content |> Floki.find(selector) |> Floki.text
  end

  defp download_images(content, path) do
    IO.puts "Скачиваем изображения"
    image_links = content |> Floki.find("#gallery li a")
      |> Floki.attribute("href")
    Enum.each(image_links, fn(image_link) ->
      spawn_link(fn ->
        IO.puts "Начинаем скачивать изображение"
        image = Tutorial.http_get(image_link)
        file_name = String.slice(image_link, -30, 30)
        File.mkdir_p!("#{path}/images/")
        File.write!("#{path}/images/#{file_name}", image)
        IO.puts "Изображение скачано #{file_name}"
      end)
    end)

  end
end
